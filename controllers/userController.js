const User = require('../models/user.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js')


// User registration w/ existing user check

module.exports.registerUser = (reqBody) => {

	// return User.find({email: reqBody.email}).then(user => {
	// 	if(user == '') {
	// 		let newUser = new User({
	// 				email: reqBody.email,
	// 				password: bcrypt.hashSync(reqBody.password, 10),
					
	// 		})

	// 		return newUser.save().then((user, error) => {
	// 			if(error) {
	// 				return "Registration failed";
	// 			}
	// 			else{
	// 				return "Registration successful!";
	// 			}

	// 		})
	// 	}
	// 	else {
	// 		return "email already exists"
	// 	}
	// })

	let newUser = new User({	
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),


	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		}
		else{
			return true;
		}

	})
}

// User login

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result=> {
		if(result == null) {
			return false;
		}
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)};
			}
			else {
				return false;
			}
		}
	})
}

// Retrieve user details

// module.exports.getProfile = (reqBody) => {
// 	return User.findOne({_id: reqBody.id}).then(result => {
// 		// result.password = '******';
// 		// return result;
		
// 		if (result == null) {
// 			return false
// 		} else {
// 			result.password = "*****"

// 			// Returns the user information with the password as an empty string or asterisk.
// 			return result
// 		}
// 	})
// }

module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};


module.exports.getUserDetails = (userId, data) => {
	return User.findById(userId).then(result => {
		result.password = '********'
		return result

	})
}

// Set User as admin (Admin Only)

// module.exports.upgradeUser = (userId, data) => {
// 	if(data.isAdmin == true) {
// 		return User.findByIdAndUpdate(userId, {
// 			isAdmin: true
// 		}
// 		).then((updateProduct, err) => {
// 			if (err) {
// 				return "Failed to upgrade User";
// 			}
// 			return "User has successfully been upgraded!";
// 		})
// 	}
// 	else {
// 		let message = Promise.resolve('User must be ADMIN to access this');
// 		return message.then(value => value);
// 	}
// }

module.exports.upgradeUser = (userId, data) => {
	if(data.isAdmin == true) {
		return User.findById(userId).then(result => {
			if (result.isAdmin == true) {
				return "User is already admin"
			} else {
				return User.findByIdAndUpdate(userId, {
					isAdmin: true
				}	
				).then((user, err) => {
					if (err) {
						return "Failed to upgrade User";
					}
					return "User has successfully been upgraded!";
				})
			}
		})
	}
	else {
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then(value => value);
	}
}

module.exports.checkEmailExist = (reqBody) => {

	// '.find' - a mongoose crud operation (query) to find a field value from a collection
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		}
		else
		{
			return false;
		}
	})
}