const Order = require('../models/order.js');
const Product = require('../models/product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js')


/*module.exports.showOrder = (data) => {
	console.log(data.isAdmin)
	if (data.isAdmin == true){
		return Order.find({}).then(result => result)
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then(value => value)
	}


}*/

module.exports.showOrder = (data) => {
	return Order.find({}).then(result => result)
}

/*
	Basic create order
	module.exports.createOrder = (data) => {
		
		return Product.find({_id: data.productId}).then(product => {
			if (data.isAdmin == false){
				let price = product[0].price
				let total 
				total = price * data.quantity

				let newOrder = new Order ({
					products: [{
						productId: data.productId,
						quantity: data.quantity}],
						userId: data.userId,
						totalAmount: total
				})

				return newOrder.save().then((newOrder, err) => {
					if (err) {
						return err;
					}
					else {
						return newOrder;
					}
				})

			}
			else{
				let message = Promise.resolve('User must be a regular user to create an order');
				return message.then(value => value)
			}
		})
		
	}
*/

/*module.exports.createOrder = (data) => {
	if(data.isAdmin == false) {
		return Product.find({_id: data.productId}).then(product => {
			if(product == '') {
				return "Please enter a valid product id"
			} else if (product[0].isActive == false) {
				return "Product is inactive"
			} else{
				let price = product[0].price
				let productName = product[0].name
				let total 
				total = price * data.quantity

				let newOrder = new Order ({
					products: [{
						name: productName,
						productId: data.productId,
						quantity: data.quantity}],
						userId: data.userId,
						totalAmount: total
				})

				return newOrder.save().then((newOrder, err) => {
					if (err) {
						return err;
					} else {
						return newOrder;
					}
				})
			}


		})
	} else {
		let message = Promise.resolve('User must be a regular user to create an order');
		return message.then(value => value)
	}

	
}*/

module.exports.createOrder = (data) => {
	return Product.find({_id: data.productId}).then(product => {
		let price = product[0].price
				let productName = product[0].name
				let total 
				total = price * data.quantity

				let newOrder = new Order ({
					products: [{
						name: productName,
						productId: data.productId,
						quantity: data.quantity}],
						userId: data.userId,
						totalAmount: total
				})

				console.log(newOrder) 

				return newOrder.save().then((newOrder, err) => {
					if (err) {
						return false;
					} else {
						return true;
					}
				})

	})
}

module.exports.myOrders = (data) => {
	if(data.isAdmin == false) {
		return Order.find({userId: data.userId})
	}
	else {
		let message = Promise.resolve('This is only for regular users!');
		return message.then(value => value);
	}
}

