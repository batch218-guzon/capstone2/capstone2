const Product = require('../models/product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js')

// module.exports.createProduct = (data) => {
// 	if(data.isAdmin == true) {
// 		let newProduct = new Product ({
// 			name: data.product.name,
// 			description:data.product.description,
// 			price: data.product.price,
// 		})

// 		return newProduct.save().then((newProduct, error)=>{
// 			if(error) {
// 				return error;
// 			}else {
// 				return newProduct;
// 			}
// 		})
		
// 	}
// 	else {
// 		let message = Promise.resolve('User must be ADMIN to access this');
// 		return message.then(value => value);
// 	}
// }

module.exports.createProduct = (reqBody) => {
	let newProduct = new Product ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((newProduct, error)=>{
		if(error) {
			return false;
		}else {
			return true;
		}
	})

}

module.exports.getActiveProduct = () => {
	return Product.find({isActive:true}).then(result => result)
}

module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => result)
}

module.exports.updateProduct =(productId, reqBody) => {
	/*if(data.isAdmin == true) {
		return Product.findByIdAndUpdate(productId, {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		}
		).then((updateProduct, err) => {
			if (err) {
				return "Failed to update product";
			} 
			return "Product successfully updated!";
		})
	}
	else {
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then(value => value);
	}*/

	return Product.findByIdAndUpdate(productId, {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	).then((updateProduct, err) => {
		if (err) {
			return false;
		} 
		return true;
	})
}

// module.exports.archiveProduct = (productId, data) => {
// 	if(data.isAdmin == true){
// 		return Product.findByIdAndUpdate(productId, {
// 			isActive: false
// 		}
// 		).then((updateProduct, err) => {
// 			if (err) {
// 				return "Failed to archive product"
// 			}
// 			return "Product has successfully been archived!"
// 		})
// 	}
// 	else {
// 		let message = Promise.resolve('User must be ADMIN to access this');
// 		return message.then(value => value);
// 	}
// }

module.exports.archiveProduct = (productId, reqBody) => {
	return Product.findByIdAndUpdate(productId, {
			isActive: false
		}
		).then((updateProduct, err) => {
			if (err) {
				return false
			}
				return true
		})
}

module.exports.activateProduct = (productId, reqBody) => {

	console.log(productId)
	return Product.findByIdAndUpdate(productId, {
			isActive: true
		}
		).then((updateProduct, err) => {
			if (err) {
				return false
			}
				return true
		})
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => result)
}

module.exports.checkProductExist = (reqBody) => {
	return Product.find({name: reqBody.name}).then(result => {
		console.log(result)

		if(result.length > 0) {

			return  {result:result[0], exist:true};
		}
		else
		{
			return false;
		}
	})
}