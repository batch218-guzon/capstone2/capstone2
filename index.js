const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require('./routes/userRoute.js');
const productRoutes = require('./routes/productRoute.js');
const orderRoutes = require('./routes/orderRoute.js');

const app = express();



app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);


mongoose.connect('mongodb+srv://admin:admin@capstone2.dtzhuu8.mongodb.net/batch218-capstone2?retryWrites=true&w=majority', {
	useNewUrlParser:true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to Guzon-Mongo DB Atlas.'));

app.listen(process.env.PORT || 4000, () => {console.log(`API is now online on port ${process.env.PORT || 4000}`)});