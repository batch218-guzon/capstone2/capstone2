const mongoose = require('mongoose');
const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "UserId is required"]
	},
	products: [
		{
			name: {
				type: String,
				required: [true, 'Product name is required']
			},
			productId: {
				type: String,
				required: [true, 'Product Id is required']
			},
			quantity: {
				type: Number,
				required: [true, 'Product quantity is required']
			}
		}
	],
	totalAmount: {
		type: Number,
		default: null
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model('Order', orderSchema);