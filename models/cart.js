const mongoose = require('mongoose');
const cartSchema = new mongoose.Schema({
	productOrder: [
		{	
			productId: {
				type: String,
				required: [true, 'Product Id is required']
			}
			productName: {
				type: String,
				required: [true, 'Product name is required']
			},
			quantity: {
				type: Number
				required: [true, 'Product quantity is required']
			},
			subtotal: {
				type: Number,
				required: [true, 'Subtotal is required']
			}
		}
	],
	total: {
		type: Number,
		required: [true, 'Total is required']
	}
	orderId: {
		type: String,
		required: [true, 'Order Id is required']
	}
})

module.exports = mongoose.model('Cart', cartSchema);
