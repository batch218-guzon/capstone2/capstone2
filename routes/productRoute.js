const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController.js');
const auth = require("../auth.js");

router.get('/showAll', (req, res) => {
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController))
});

// router.post('/create', auth.verify, (req, res) => {
// 	const data = {
// 		product: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}

// 	productController.createProduct(data).then(resultFromController=>res.send(resultFromController))
// });

router.post('/create', (req, res) => {
	productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))
});

router.get('/active', (req, res) => {
	productController.getActiveProduct().then(resultFromController=>res.send(resultFromController))
});

router.get('/:productId', (req, res) => {
	productController.getProduct(req.params.productId).then(resultFromController=>res.send(resultFromController))
});

// router.patch('/:productId', auth.verify, (req, res) => {
// 	const data = {
// 		product: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin

// 	}
// 	productController.updateProduct(req.params.productId, data).then(resultFromController=> res.send(resultFromController))
// });

router.patch('/:productId', (req, res) => {
	productController.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController))
})

// router.patch('/:productId/archive', auth.verify, (req, res)=> {
// 	const data = {
// 		product: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}
// 	productController.archiveProduct(req.params.productId, data).then(resultFromController=> res.send(resultFromController))
// });

router.patch('/:productId/archive', (req, res) => {
	productController.archiveProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController))
})

router.patch('/:productId/activate', (req, res) => {
	productController.activateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController))
})

router.post('/checkProduct', (req, res) => {
	productController.checkProductExist(req.body).then(resultFromController => res.send(resultFromController))
});

module.exports = router;