const express = require('express');
const router = express.Router();
const User = require('../models/user.js');
const userController = require('../controllers/userController.js');
const auth = require("../auth.js");

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.get('/details', (req, res) => {
	// userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))

	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
})

router.get('/:userId/userDetails', auth.verify, (req, res) => {
	userController.getUserDetails(req.params.userId, req.body).then(resultFromController => res.send(resultFromController))
})

router.patch('/:userId/setAsAdmin', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		user: req.body
	}
	userController.upgradeUser(req.params.userId, data).then(resultFromController => res.send(resultFromController))
})

router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

module.exports = router;
