const express = require('express');
const router = express.Router();
const User = require('../models/order.js');
const orderController = require('../controllers/orderController.js');
const auth = require("../auth.js");

// router.post('/checkout', auth.verify, (req, res) => {
// 	const data = {
// 		productId: req.body.productId,
// 		quantity: req.body.quantity,
// 		userId: auth.decode(req.headers.authorization).id, 
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
// 	}
// 	orderController.createOrder(data).then(resultFromController=> res.send(resultFromController))
// });

router.post('/checkout', (req, res) => {
	const data = {
		productId: req.body.productId,
		quantity: req.body.quantity,
		userId: req.body.userId,
		isAdmin: req.body.isAdmin
	}
	console.log(data)

	orderController.createOrder(req.body).then(resultFromController=> res.send(resultFromController))
});

// router.get('/', auth.verify, (req, res) => {
// 	const data = {
// 		user: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}

// 	orderController.showOrder(data).then(resultFromController=> res.send(resultFromController))
// })

router.get('/', (req, res) => {
	orderController.showOrder(req.body).then(resultFromController=> res.send(resultFromController))
})

router.get('/myOrders', auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	orderController.myOrders(data).then(resultFromController => res.send(resultFromController))
})
module.exports = router;